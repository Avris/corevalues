import TinyTranslator from 'avris-tinytranslator';
import { ref } from 'vue';

import en from '../translations/en.suml';
import pl from '../translations/pl.suml';

const languages = {
    'en': en,
    'pl': pl,
};

export default defineNuxtPlugin(nuxtApp => {
    const translator = new TinyTranslator(languages, ref('en'));
    nuxtApp.provide('translator', translator);
    nuxtApp.provide('t', translator.translate.bind(translator));
})
