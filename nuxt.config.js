import sumlLoader from 'suml-loader';

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    runtimeConfig: {
        public: {
            baseUrl: process.env.NUXT_PUBLIC_BASE_URL || 'http://localhost:3000',
            debug: process.env.NUXT_PUBLIC_DEBUG || '',
        },
        ipvxUrl: process.env.NUXT_IPVX_URL || 'http://127.0.0.1:8111',
    },
    vite: {
        plugins: [{
            name: 'suml-loader',
            transform(src, id) {
                if (id.endsWith('.suml')) {
                    return {
                        code: sumlLoader(src),
                        map: null,
                    }
                }
            },
        }],
    },
    modules: [
        '@nuxtjs/plausible',
    ],
    plausible: {
        domain: 'corevalues.avris.it',
        apiHost: 'https://plausible.avris.it',
    },
})
