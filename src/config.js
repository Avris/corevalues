export const VALUES = [
    'love', 'relationships', 'friendship', 'family', 'citizenship', 'community', 'contribution', 'productivity', 'competence', 'wisdom',
    'knowledge', 'curiosity', 'intuition', 'truth', 'resourcefulness', 'education', 'popularity', 'reputation', 'fame', 'influence',
    'entertainment', 'joy', 'happiness', 'fun', 'wealth', 'inclusivity', 'pleasure', 'humour', 'optimism', 'openness',
    'acceptance', 'loyalty', 'tolerance', 'compassion', 'consideration', 'trust', 'sensitivity', 'tenderness', 'authenticity', 'patience',
    'pride', 'beauty', 'justice', 'righteousness', 'humility', 'environment', 'harmony', 'balance', 'politeness', 'caution',
    'peace', 'simplicity', 'hope', 'support', 'thoughtfulness', 'gratitude', 'kindness', 'generosity', 'purity', 'spirituality',
    'faith', 'religion', 'safety', 'stability', 'health', 'comfort', 'independence', 'freedom', 'self-awareness', 'enthusiasm',
    'growth', 'passion', 'creativity', 'perfection', 'success', 'mission', 'engagement', 'achievement', 'dreams', 'challenge',
    'risk', 'spontaneity', 'adventure', 'elasticity', 'authority', 'leadership', 'respect', 'responsibility', 'status', 'recognition',
    'honour', 'dignity', 'integrity', 'trustworthiness', 'honesty', 'nobility', 'fidelity', 'self-respect', 'bravery', 'assertiveness',
];

export const STEPS = [ 18, 12, 6 ];

export const CORE_COUNT = STEPS[STEPS.length - 1];

export const MIN_SIZE_STATS = 100;
