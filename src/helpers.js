import { convertBase } from 'baseroo'
import { VALUES, CORE_COUNT } from "~/src/config";

class InvalidValuesFormat extends Error {
    constructor(values) {
        super(`Values format validation failed (${values.join(', ')})`);
    }
}

class CodeManager {
    encode(values) {
        if (!this.validate(values)) { throw new InvalidValuesFormat(values); }

        return convertBase(
            values
                .map(v => VALUES.indexOf(v).toString().padStart(2, '0'))
                .join(''),
            10,
            64
        ).toString().replace(/\//g, '-');
    }

    decode(code) {
        const values = convertBase(code.replace(/-/g, '/'), 64, 10)
            .toString()
            .padStart(CORE_COUNT * 2, '0')
            .match(/.{1,2}/g)
            .map(v => VALUES[parseInt(v)]);

        if (!this.validate(values)) { throw new InvalidValuesFormat(values); }

        return values;
    }

    validate(values) {
        return values.length === CORE_COUNT
            && new Set(values).size === values.length
            && values.filter(v => VALUES.includes(v));
    }
}
export const codeManager = new CodeManager();

// https://stackoverflow.com/a/46545530/3297012
// https://stackoverflow.com/a/53758827/3297012
export const shuffled = (array, seed) => {
    const seededRandom = () => {
        let x = Math.sin(seed++) * 10000;
        return x - Math.floor(x);
    }

    return array
        .map(value => ({ value, sort: seed === undefined ? Math.random() : seededRandom() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value)
}

export const sortByObjectValues = dict =>
    Object.fromEntries(
        Object.entries(dict)
            .sort((a, b) => b[1] - a[1])
    );
