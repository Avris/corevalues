# corevalues

homepage: [corevalues.avris.it](http://corevalues.avris.it)

## setup

```bash
make install
```

## development server

start the development server on `http://localhost:3000`

```bash
make run
```

## production

```bash
make deploy
```

## author

 - andrea vos
 - [avris.it](https://avris.it)
 - license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
