const SQL = require('sql-template-strings');
const dbConnection = require('./db');

(async () => {
    const db = await dbConnection();
    await db.get(SQL`
        CREATE TABLE IF NOT EXISTS responses (
            fingerprint TEXT PRIMARY KEY,
            response TEXT NOT NULL,
            country TEXT NULL
        )`);
})();
