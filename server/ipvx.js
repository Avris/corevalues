import fetch from 'node-fetch';

export default async (ip) => {
    const { ipvxUrl } = useRuntimeConfig();
    try {
        const country = await (await fetch(ipvxUrl + '/' + ip)).text();

        return country === 'XX' ? null : country;
    } catch {
        return null;
    }
}
