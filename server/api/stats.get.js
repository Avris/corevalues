import dbConnection from '../db';
import SQL from 'sql-template-strings';
import { sortByObjectValues } from '../../src/helpers';
import { MIN_SIZE_STATS } from '../../src/config';

export default defineEventHandler(async event => {
    const db = await dbConnection();
    let count = 0;
    const heatmap = {};
    for (let {response} of await db.all(SQL`SELECT response FROM responses`)) {
        count++;
        for (let value of response.split(',')) {
            heatmap[value] = (heatmap[value] || 0) + 1;
        }
    }

    if (count < MIN_SIZE_STATS) {
        return { count: 0, heatmap: {}};
    }

    return { count, heatmap: sortByObjectValues(heatmap) };
});
