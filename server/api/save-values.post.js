import { fingerprint, ip } from 'tinyfingerprint';
import ipvx from "../ipvx";
import dbConnection from '../db';
import SQL from 'sql-template-strings';
import { VALUES, CORE_COUNT } from "../../src/config.js";

export default defineEventHandler(async event => {
    const { values } = await readBody(event);

    if (!Array.isArray(values) || values.length !== CORE_COUNT || values.filter(v => VALUES.includes(v)).length !== CORE_COUNT) {
        throw createError({
            statusCode: 400,
            statusMessage: 'unrecognised values',
        })
    }

    const db = await dbConnection();
    await db.get(SQL`
        INSERT OR REPLACE INTO responses(fingerprint, response, country)
        VALUES (
            ${fingerprint(event.node.req)},
            ${values.join(',')},
            ${await ipvx(ip(event.node.req))}
        );
    `);

    return { status: 'OK' };
});
