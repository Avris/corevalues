import { fingerprint } from 'tinyfingerprint';
import {convertBase} from "baseroo";

export default defineEventHandler((event) => {
    const fp = fingerprint(event.node.req);
    const seed = convertBase(fp.substring(0, 8), 16, 10);

    event.context.seed = seed;
    event.node.res.setHeader('Set-Cookie', [`seed=${seed}; Path=/;`]);
});
