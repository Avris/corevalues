install:
	pnpm install
	node server/schema.js

run: install
	pnpm run dev

deploy: install
	pnpm run build
